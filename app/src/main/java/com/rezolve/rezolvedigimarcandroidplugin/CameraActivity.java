package com.rezolve.rezolvedigimarcandroidplugin;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

public class CameraActivity extends AppCompatActivity {

    private Camera camera;
    private Preview preview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        // Create camera
        this.camera = getCamera();
        // Create preview
        this.preview = new Preview(this, this.camera);
        FrameLayout previewLayout = (FrameLayout) findViewById(R.id.preview);
        previewLayout.addView(preview);

        // Set view components
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(R.string.app_name);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    /**
     * Check if the device has a camera.
     *
     * @param context
     * @return boolean
     */
    private boolean checkCameraHardware(Context context){
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            return true;
        }

        return false;
    }

    /**
     * Get the device camera
     * @return
     */
    private Camera getCamera(){
        Camera c = null;

        try {
            c = Camera.open();
        } catch(Exception e){
            Log.d("ERROR", "Error accessing the device camera: "+e.getMessage());
        }

        return c;
    }

}
