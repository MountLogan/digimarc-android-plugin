package com.rezolve.rezolvedigimarcandroidplugin;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

/**
 * Created by Shavit on 1/26/17.
 */

public class Preview extends SurfaceView implements SurfaceHolder.Callback {
    private SurfaceHolder surfaceHolder;
    private Camera camera;

    public Preview(Context context, Camera camera){
        super(context);
        this.camera = camera;

        // Add callback when created and destroyed
        this.surfaceHolder = getHolder();
        this.surfaceHolder.addCallback(this);
    }

    /**
     * When the preview is created
     * @param holder
     */
    public void surfaceCreated(SurfaceHolder holder){
        try {
            this.camera.setPreviewDisplay(surfaceHolder);
            this.camera.startPreview();
        } catch (IOException e){
            Log.d("ERROR", "Error creating camera preview: "+e.getMessage());
        }
    }

    /**
     * When the preview rotated or changed
     * @param holder
     * @param format
     * @param width
     * @param height
     */
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height){
        // Check if preview exists
        if (this.surfaceHolder.getSurface() == null){
            return;
        }

        // Stop preview before making changes
        try {
            this.camera.stopPreview();
        } catch (Exception e){
            // Pass
            // There is no preview
        }

        try {
            this.camera.setPreviewDisplay(this.surfaceHolder);
            this.camera.startPreview();
        } catch (Exception e){
            Log.d("ERROR", "Error starting camera preview: "+e.getMessage());
        }
    }

    /**
     * When the surface been destroyed
     * @param holder
     */
    public void surfaceDestroyed(SurfaceHolder holder){
        //
    }
}
